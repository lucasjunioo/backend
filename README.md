# Padrão de projeto backend

Retorna uma resposta através da requisição GET

### Pré-requisitos

```
NodeJS (Versão v12.16.3 ou acima)
```

### Instalando

Após realizar a clonagem do projeto, entre na projeto da aplicação e execute o código abaixo para instalar as dependências do webserver.

```
npm install
```

Depois de finalizar a execução do código acima, inicie a aplicação executando o código abaixo no terminal/CMD

```
npm start
```

## Estrutura do Projeto

```
Backend project
|   .gitignore
|   index.js
|   package-lock.json
|   package.json
|   README.md
|   
\---routes
        index.js
        task.routes.js
        

```

## Rotas

**EXIBIR MENSAGEM DE BOAS-VINDAS**

**GET** ```/api/task```

**200** ```OK```

**Exemplo de retorno**
```
{"status":"Hello, welcome API. This is GET global request from task."}
```


## Técnologias utilizadas

* [Express](https://expressjs.com/pt-br/) - Framework NodeJS
* [Morgan](http://expressjs.com/en/resources/middleware/morgan.html/) - Utilizado para exibição de detalhes de requisição em logs
* [Body-parser](http://expressjs.com/en/resources/middleware/body-parser.html/) - middleware utilizado para conversão de requisições
* [Nodemon](https://nodemon.io/) - Utilitário para atualizar automaticamente o webserver

## Author

* **Lucas Junio Gomes Azevedo**