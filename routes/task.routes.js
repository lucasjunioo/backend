const express = require('express');
const router = express.Router();

// General query of activity
router.get('/', async (req, res) => {
    res.json({status: 'Hello, welcome API. This is GET global request from task.'})
})

module.exports = router; 