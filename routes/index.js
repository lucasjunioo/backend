const router = require('express').Router()

// This file exist for show that is possible diferent shape to routering
router.use('/task', require('./task.routes'))

module.exports = router;