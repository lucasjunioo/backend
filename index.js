const express = require('express');

// Express: Facto standard server framework for Node.js
const app = express()
       
// Morgan is used for logging request details;
const morgan = require('morgan');
const path = require('path');
const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


// Settings
app.set('port', process.env.PORT || 3000);

// Middlewares: functions run before of create routes
app.use(morgan('dev'));
app.use(express.json());

// Routes
app.use('/api', require('./routes'));

// Redirection of routes no specification for main page
app.get('/*', function(req, res) {
	res.send("Error 404 page not found!")
  })

// Starting the server
app.listen(app.get('port'), () => {
    console.log(`Serve on port ${app.get('port')}`)
})

